(function() {
	var frame = 0;
	var frameRate = 1000;
	var frames = [];
	var ctx = null;
	var width = 500;
	var height = 500;

	var assets = [
		'http://placekitten.com/20/30',
		'http://placekitten.com/30/20',
		'http://placekitten.com/30/20',
		'http://placekitten.com/20/30'
	];
	
	var animate = function() {
		if (getQueryStringValue("debug") === "true") console.log('[Animation - current frame] ', frame);
		ctx.clearRect(0,0, width, height);
		ctx.drawImage(frames[frame], 10, 10);
		frame = (frame + 1) % assets.length;
	};

	function loadImages() {
		for (var i = 0; i < assets.length; i++) {
			frames.push(new Image());
			frames[i].src = assets[i];
			frames[i].onload = onLoadImage;
		};
	}

	function onLoadImage(e) {
		if (getQueryStringValue("debug") === "true") console.log('[Load Images Complete - start animation]');
		if(frames.length === assets.length) {
			setInterval(animate, frameRate);
		}
	}
	
	function getQueryStringValue(name) {
		var search = window.location.search.substr(1);
		var keyAndNames = search.split('&');
		for(var key in keyAndNames)	{
			if(keyAndNames[key].indexOf(name+"=") === 0) {
				return keyAndNames[key].split('=')[1];
			}
		}
	}

	function create () {
		var canvas = document.createElement('canvas');
		canvas.width = width;
		canvas.height = height;
		if(document.body !== null) {
			document.body.appendChild(canvas);
		}
		ctx = canvas.getContext('2d');
	}

	function init()	{
		if(getQueryStringValue("exec") === "start") {
			create();
			loadImages();
		}
	}

	window.onload = function(){
		init();		
	};
})();